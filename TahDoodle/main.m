//
//  main.m
//  TahDoodle
//
//  Created by Antanas Domarkas on 2013-12-04.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
