//
//  BNRDocument.h
//  TahDoodle
//
//  Created by Antanas Domarkas on 2013-12-04.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BNRDocument : NSDocument <NSTableViewDataSource>
{
    NSMutableArray *todoItems;
    IBOutlet NSTableView *itemTableView;
    IBOutlet NSButton *deleteButton;
}

- (IBAction)createNewItem:(id)sender;
- (IBAction)deleteSelectedItem:(id)sender;
- (void)itemTableViewSelectionChanged:(NSNotification *)notification;

@end
